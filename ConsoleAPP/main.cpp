#include <iostream>
#include <string>
#include "Player.h"

auto compare = [](Player &a, Player &b) {   //сортировка
    return  a.getScore() > b.getScore();
};

void showPlayers(Player players[], int length){
    for(int i = 0; i < length; i++){
        players[i].printData();
    }
}

int main(int argc, const char * argv[]) {
    std::cout << "How many players?";
    int pCount;
    std::cin >> pCount;
    std::cin.ignore();

    Player* players = new Player[pCount];
    for(int i = 0; i < pCount; i++){
        std::cout << "What is name of " << (i+1) << " player?";
        std::string name;
        std::getline(std::cin, name);

        std::cout << "What about score?";
        std::string score;
        std::getline(std::cin, score);

        players[i].setData(name, std::stod(score));
    }

    std::sort(players, players + pCount, compare);
    showPlayers(players, pCount);
    delete[] players;
}