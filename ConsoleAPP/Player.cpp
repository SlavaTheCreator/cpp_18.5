#include "Player.h"
#include <string>
#include <iostream>

void Player::printData(){
    cout << "Name: " << name << ". Score: " << score << '\n';
}
void Player::setData(string name_, double score_) {
    this->name = name_;
    this->score = score_;
}

double Player::getScore() {
    return this->score;
}