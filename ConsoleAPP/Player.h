#ifndef CONSOLEAPP_PLAYER_H
#define CONSOLEAPP_PLAYER_H

#include <string>
#include <iostream>
using namespace std;
class Player {
protected:
    string name;
    double score = 0;
public:
    Player() : name("Player"){};
    Player(const char *name_, double score_) : name(name_), score(score_)
    {}

    void printData();
    void setData(string name_, double score_);
    double getScore();
};


#endif //CONSOLEAPP_PLAYER_H
